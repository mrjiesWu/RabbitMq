package com.wu.rbmq;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RbmqApplicationTests {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test  //hello
    void contextLoads() {
        rabbitTemplate.convertAndSend("hello","hello world!!!!");
    }

    @Test //work
    void testWork(){
        for (int i = 0; i < 100; i++) {
            rabbitTemplate.convertAndSend("work","work----"+i);
        }
    }


    @Test //fanout
    void testFanout(){
        rabbitTemplate.convertAndSend("logs","","测试广播!!!");
    }

    @Test //direct
    void testDirect(){
        rabbitTemplate.convertAndSend("pros","log","路由!!");
    }

    @Test  //topic
    void testTopic(){
        rabbitTemplate.convertAndSend("top","user.name.66","小米");
    }

}
