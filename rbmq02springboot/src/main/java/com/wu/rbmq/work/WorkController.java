package com.wu.rbmq.work;

import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-10 09:36
 **/

@Component
@RabbitListener(queuesToDeclare = @Queue("work"))
public class WorkController {

    @RabbitHandler
    public void receivel(String msg){
        System.out.println("work消费::"+msg);
    }
}
