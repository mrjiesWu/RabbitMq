package com.wu.rbmq.rount;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-10 10:10
 **/

@Component
public class RountController {


    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "pros",type = "direct"),
                    key = {"pro","log"}
            )
    })
    public void msg01(String msg){
        System.out.println("msg01------"+msg);
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "pros",type = "direct"),
                    key = {"log"}
            )
    })
    public void msg02(String msg){
        System.out.println("msg02------"+msg);
    }

}
