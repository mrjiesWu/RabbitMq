package com.wu.rbmq.fanout;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-10 09:48
 **/
@Component
public class FanoutController {

    @RabbitListener(bindings={
            @QueueBinding(
                    value = @Queue,//创建临时队列
                    exchange = @Exchange(value = "logs",type = "fanout") //绑定交换机
            )
    })
    public void hello(String msg){
        System.out.println("hello----"+msg);
    }


    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "logs",type = "fanout")
            )
    })
    public void work(String msg){
        System.out.println("work----"+msg);
    }
}
