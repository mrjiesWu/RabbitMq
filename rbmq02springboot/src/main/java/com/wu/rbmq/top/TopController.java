package com.wu.rbmq.top;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-10 10:20
 **/

@Component
public class TopController {

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "top",type = "topic"),
                    key = {"user.#"}
            )
    })
    public void msg01(String msg){
        System.out.println("msg01"+msg);
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(name = "top",type = "topic"),
                    key = {"user.*"}
            )
    })

    public void msg02(String msg){
        System.out.println("msg02"+msg);
    }
}
