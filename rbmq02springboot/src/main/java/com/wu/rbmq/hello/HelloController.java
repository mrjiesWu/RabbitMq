package com.wu.rbmq.hello;

import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-10 09:20
 **/
@Component
@RabbitListener(queuesToDeclare = @Queue("hello"))
public class HelloController {

    @RabbitHandler
    public void receivel(String body){
        System.out.println("hello消费--"+body);
    }

}
