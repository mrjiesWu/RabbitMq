![RabbitMQ](https://blog.csdn.net/hellozpc/article/details/81436980)


## 准备工作
- 使用页面方式添加用户,虚拟主机,配置用户可以访问的虚拟主机
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200607112216275.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzUyMzg3NQ==,size_16,color_FFFFFF,t_70)

# 6种模型

## HelloWorld模型--直连
![](https://www.rabbitmq.com/img/tutorials/python-one.png)
- 生产者
```java
public class Provider {

    public static void main(String[] args) throws IOException, TimeoutException {
        /*//创建连接mq的连接工厂对象
        ConnectionFactory connectionFactory = new ConnectionFactory();
        //设置连接rbmq的主机
        connectionFactory.setHost("ip");
        //设置端口
        connectionFactory.setPort(5672);
        //设置连接那个虚拟机
        connectionFactory.setVirtualHost("/ems");
        //设置访问虚拟主机的用户名密码
        connectionFactory.setUsername("ems");
        connectionFactory.setPassword("******");
        //获取连接对象
        Connection connection = connectionFactory.newConnection();*/

        //获取连接
        Connection connection = RbmqUtil.getConnection();

        //获取连接中通道
        Channel channel = connection.createChannel();
        //通道绑定对应的消息队列
        //参数1:队列名称,如果不存在就创建
        //参数2:定义队列特性是否要持久化,
        //参数3:exclusive 是否独占队列
        //参数4:autoDelete 是否在消费完自动删除队列
        //参数5: 额外附加参数
        channel.queueDeclare("hello",false, false,false,null);
        //发布消息
        //参数1:交换机
        //参数2:队列的名称
        //参数3:额外属性设置
        //参数3:具体内容
        channel.basicPublish("","hello",null,"hello".getBytes());

        /*//关闭通道
        channel.close();
        //关闭连接
        connection.close();*/

        RbmqUtil.closeChannelAndConnection(channel,connection);
    }

}
```
- 消费者
```java
public class Customer {

    public static void main(String[] args) throws IOException, TimeoutException {
        /*//创建连接mq的连接工厂对象
        ConnectionFactory connectionFactory = new ConnectionFactory();
        //设置连接rbmq的主机
        connectionFactory.setHost("ip");
        //设置端口
        connectionFactory.setPort(5672);
        //设置连接那个虚拟机
        connectionFactory.setVirtualHost("/ems");
        //设置访问虚拟主机的用户名密码
        connectionFactory.setUsername("ems");
        connectionFactory.setPassword("******");
        //获取连接对象
        Connection connection = connectionFactory.newConnection();*/

        //获取连接
        Connection connection = RbmqUtil.getConnection();
        //获取连接中通道
        Channel channel = connection.createChannel();

        channel.queueDeclare("hello",false, false,false,null);

        channel.basicConsume("hello",true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(LocalDateTime.now()+"--"+new String(body));
            }
        });

    }
}
```
- 封装工具类

```java
package com.wu.utils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-07 14:36
 **/

public class RbmqUtil {

    private static  ConnectionFactory connectionFactory;

    static {
        //创建连接mq的连接工厂对象  -- ConnectionFactory重量级资源  类加载执行  只执行一次
        connectionFactory = new ConnectionFactory();
        //设置连接rbmq的主机
        connectionFactory.setHost("ip");
        //设置端口
        connectionFactory.setPort(5672);
        //设置连接那个虚拟机
        connectionFactory.setVirtualHost("/ems");
        //设置访问虚拟主机的用户名密码
        connectionFactory.setUsername("ems");
        connectionFactory.setPassword("***");
    }

    //创建连接
    public static Connection getConnection() {
        Connection connection = null;
        try {
            //获取连接对象
            connection = connectionFactory.newConnection();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return connection;
    }

    //关闭连接和通道
    public static void closeChannelAndConnection(Channel channel, Connection connection) {

        try {
            channel.close();
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

}

```
## Work queues--任务队列 
![](https://www.rabbitmq.com/img/tutorials/python-two.png)

- 平分消费
```java
public class Provider {

    public static void main(String[] args) throws IOException {

        Connection connection = RbmqUtil.getConnection();

        Channel channel = connection.createChannel();

        channel.queueDeclare("work",true,false,false,null);

        for (int i = 0; i <100; i++) {
            channel.basicPublish("","work",null,(i+":发送了消息").getBytes());
        }

        RbmqUtil.closeChannelAndConnection(channel,connection);

    }
}

public class Customer01 {

    public static void main(String[] args) throws IOException {
        Connection connection = RbmqUtil.getConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare("work",true,false,false,null);
        channel.basicConsume("work",true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(LocalDateTime.now()+"----消费者:01:"+new String(body));
            }
        });
    }
}

public class Customer02 {

    public static void main(String[] args) throws IOException {
        Connection connection = RbmqUtil.getConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare("work",true,false,false,null);
        channel.basicConsume("work",true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(LocalDateTime.now()+"----消费者:02:"+new String(body));
            }
        });
    }
}
```

- 消息确认
```java
public class Customer01 {

    public static void main(String[] args) throws IOException {
        Connection connection = RbmqUtil.getConnection();
        Channel channel = connection.createChannel();

        //每次只消费一个通知
        channel.basicQos(1);

        channel.queueDeclare("work",true,false,false,null);

//        channel.basicConsume("work",true,new DefaultConsumer(channel){
        channel.basicConsume("work",false,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(LocalDateTime.now()+"----消费者:01:"+new String(body));
                //确认消息
                //参数1: 确认队列中哪个具体消息  参数2: 是否开启多个消息的同时确认
                channel.basicAck(envelope.getDeliveryTag(),false);
            }
        });
    }
}


public class Customer02 {

    public static void main(String[] args) throws IOException {
        Connection connection = RbmqUtil.getConnection();
        Channel channel = connection.createChannel();

        //每次只消费一个通知
        channel.basicQos(1);

        channel.queueDeclare("work",true,false,false,null);

        //参数1: 队列名称 参数2:消息自动确定 true --消费者自动向rabbitmq确认消息消费 :  false 不会自动确认消息
//        channel.basicConsume("work",true,new DefaultConsumer(channel){
        channel.basicConsume("work",false,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(LocalDateTime.now()+"----消费者:02:"+new String(body));
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //确认消息
                //参数1: 确认队列中哪个具体消息  参数2: 是否开启多个消息的同时确认
                channel.basicAck(envelope.getDeliveryTag(),false);
            }
        });
    }
}
```

## Publish/Subscribe 发布订阅
![](https://www.rabbitmq.com/img/tutorials/python-three.png)
- fanout
```java
public class Provider {
    public static void main(String[] args) throws IOException {
        Connection connection = RbmqUtil.getConnection();
        Channel channel = connection.createChannel();
        //声明交换机
        //参数1:交换机的名称  参数2: fanout为广播
        channel.exchangeDeclare("logs","fanout");
        //发布消息
        channel.basicPublish("logs","",null,"这是一条广播信息!!!".getBytes());

        RbmqUtil.closeChannelAndConnection(channel,connection);

    }
}



public class Customer01 {

    public static void main(String[] args) throws IOException {
        Connection connection = RbmqUtil.getConnection();
        Channel channel = connection.createChannel();
        //绑定交换机
        channel.exchangeDeclare("logs","fanout");
        //创建临时队列
        String queue = channel.queueDeclare().getQueue();
        //将临时队列绑定exchange
        channel.queueBind(queue,"logs","");
        //处理消息
        channel.basicConsume(queue,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("消费者01---"+new String(body));
            }
        });
    }
}
```
## 路由 Routing
![](https://www.rabbitmq.com/img/tutorials/python-four.png)

- direct
```java
public class Provider {

    public static void main(String[] args) throws IOException {

        Connection connection = RbmqUtil.getConnection();

        Channel channel = connection.createChannel();
        channel.exchangeDeclare("logs_direct","direct");
        String routKey = "news";
        channel.basicPublish("logs_direct",routKey,null,"这是一条日志信息".getBytes());

        RbmqUtil.closeChannelAndConnection(channel,connection);

    }
}

public class Customer01 {
    public static void main(String[] args) throws IOException {
        Connection connection = RbmqUtil.getConnection();
        Channel channel = connection.createChannel();

        String exchange = "logs_direct";
        String routKey = "log";
        channel.exchangeDeclare(exchange,"direct");
        //创建临时队列
        String queue = channel.queueDeclare().getQueue();
        channel.queueBind(queue,exchange,routKey);

        channel.basicConsume(queue,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("消费者--1--"+new String(body));
            }
        });

    }
}
```

## 动态路由 Topics
- topic
```java
public class Provider {
    public static void main(String[] args) throws IOException {
        Connection connection = RbmqUtil.getConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("topics","topic");
        channel.basicPublish("topics","user.save.2",null,"这是一条tops信息".getBytes());
        RbmqUtil.closeChannelAndConnection(channel,connection);
    }
}


public class Customer01 {
    public static void main(String[] args) throws IOException {
        Connection connection = RbmqUtil.getConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("topics", "topic");
        //创建临时队列
        String queue = channel.queueDeclare().getQueue();
        //    *  匹配一个单词
        channel.queueBind(queue, "topics", "user.*");
        channel.basicConsume(queue,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("-----" + new String(body));
            }
        });
    }
}
```
## Springboot整合

```yaml
spring:
  application:
    name: rabbit_mq
  rabbitmq:
    host: ***.**
    password: ...
    port: 5672
    username: ems
    virtual-host: /ems
```


### hello
```java
@Test  //hello
void contextLoads() {
    rabbitTemplate.convertAndSend("hello","hello world!!!!");
}

@Component
@RabbitListener(queuesToDeclare = @Queue("hello"))
public class HelloController {

    @RabbitHandler
    public void receivel(String body){
        System.out.println("hello消费--"+body);
    }

}
```
### 广播
```java
@Test //fanout
void testFanout(){
    rabbitTemplate.convertAndSend("logs","","测试广播!!!");
}


@Component
public class FanoutController {

    @RabbitListener(bindings={
            @QueueBinding(
                    value = @Queue,//创建临时队列
                    exchange = @Exchange(value = "logs",type = "fanout") //绑定交换机
            )
    })
    public void hello(String msg){
        System.out.println("hello----"+msg);
    }


    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "logs",type = "fanout")
            )
    })
    public void work(String msg){
        System.out.println("work----"+msg);
    }
}
```

### 路由
```java
@Test //direct
void testDirect(){
    rabbitTemplate.convertAndSend("pros","log","路由!!");
}

@Component
public class RountController {

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "pros",type = "direct"),
                    key = {"pro","log"}
            )
    })
    public void msg01(String msg){
        System.out.println("msg01------"+msg);
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "pros",type = "direct"),
                    key = {"log"}
            )
    })
    public void msg02(String msg){
        System.out.println("msg02------"+msg);
    }

}
```

### 动态路由
```java
@Test  //topic
void testTopic(){
    rabbitTemplate.convertAndSend("top","user.name.66","小米");
}

@Component
public class TopController {

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "top",type = "topic"),
                    key = {"user.#"}
            )
    })
    public void msg01(String msg){
        System.out.println("msg01"+msg);
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(name = "top",type = "topic"),
                    key = {"user.*"}
            )
    })

    public void msg02(String msg){
        System.out.println("msg02"+msg);
    }
}
```