package com.wu.utils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-07 14:36
 **/

public class RbmqUtil {

    private static  ConnectionFactory connectionFactory;

    static {
        //创建连接mq的连接工厂对象  -- ConnectionFactory重量级资源  类加载执行  只执行一次
        connectionFactory = new ConnectionFactory();
        //设置连接rbmq的主机
        connectionFactory.setHost("116.62.131.109");
        //设置端口
        connectionFactory.setPort(5672);
        //设置连接那个虚拟机
        connectionFactory.setVirtualHost("/ems");
        //设置访问虚拟主机的用户名密码
        connectionFactory.setUsername("ems");
        connectionFactory.setPassword("123");
    }

    //创建连接
    public static Connection getConnection() {
        Connection connection = null;
        try {
            //获取连接对象
            connection = connectionFactory.newConnection();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return connection;
    }

    //关闭连接和通道
    public static void closeChannelAndConnection(Channel channel, Connection connection) {

        try {
            channel.close();
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

}
