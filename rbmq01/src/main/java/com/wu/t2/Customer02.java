package com.wu.t2;

import com.rabbitmq.client.*;
import com.wu.utils.RbmqUtil;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-07 16:52
 **/

public class Customer02 {

    public static void main(String[] args) throws IOException {
        Connection connection = RbmqUtil.getConnection();
        Channel channel = connection.createChannel();

        //每次只消费一个通知
        channel.basicQos(1);

        channel.queueDeclare("work",true,false,false,null);

        //参数1: 队列名称 参数2:消息自动确定 true --消费者自动向rabbitmq确认消息消费 :  false 不会自动确认消息
//        channel.basicConsume("work",true,new DefaultConsumer(channel){
        channel.basicConsume("work",false,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(LocalDateTime.now()+"----消费者:02:"+new String(body));
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //确认消息
                //参数1: 确认队列中哪个具体消息  参数2: 是否开启多个消息的同时确认
                channel.basicAck(envelope.getDeliveryTag(),false);
            }
        });
    }
}
