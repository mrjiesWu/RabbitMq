package com.wu.t2;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.wu.utils.RbmqUtil;

import java.io.IOException;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-07 16:46
 **/

public class Provider {

    public static void main(String[] args) throws IOException {

        Connection connection = RbmqUtil.getConnection();

        Channel channel = connection.createChannel();

        channel.queueDeclare("work",true,false,false,null);

        for (int i = 0; i <100; i++) {
            channel.basicPublish("","work",null,(i+":发送了消息").getBytes());
        }

        RbmqUtil.closeChannelAndConnection(channel,connection);

    }
}
