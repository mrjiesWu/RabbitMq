package com.wu.t1;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.wu.utils.RbmqUtil;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-07 13:37
 *
 *  生产消息
 **/

public class Provider {

    public static void main(String[] args) throws IOException, TimeoutException {
        /*//创建连接mq的连接工厂对象
        ConnectionFactory connectionFactory = new ConnectionFactory();
        //设置连接rbmq的主机
        connectionFactory.setHost("116.62.131.109");
        //设置端口
        connectionFactory.setPort(5672);
        //设置连接那个虚拟机
        connectionFactory.setVirtualHost("/ems");
        //设置访问虚拟主机的用户名密码
        connectionFactory.setUsername("ems");
        connectionFactory.setPassword("123");
        //获取连接对象
        Connection connection = connectionFactory.newConnection();*/

        //获取连接
        Connection connection = RbmqUtil.getConnection();

        //获取连接中通道
        Channel channel = connection.createChannel();
        //通道绑定对应的消息队列
        //参数1:队列名称,如果不存在就创建
        //参数2:定义队列特性是否要持久化,
        //参数3:exclusive 是否独占队列
        //参数4:autoDelete 是否在消费完自动删除队列
        //参数5: 额外附加参数
        channel.queueDeclare("hello",false, false,false,null);
        //发布消息
        //参数1:交换机
        //参数2:队列的名称
        //参数3:额外属性设置
        //参数3:具体内容
        channel.basicPublish("","hello",null,"hello".getBytes());

        /*//关闭通道
        channel.close();
        //关闭连接
        connection.close();*/

        RbmqUtil.closeChannelAndConnection(channel,connection);
    }

}
