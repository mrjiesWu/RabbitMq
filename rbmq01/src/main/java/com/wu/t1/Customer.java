package com.wu.t1;

import com.rabbitmq.client.*;
import com.wu.utils.RbmqUtil;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.concurrent.TimeoutException;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-07 14:24
 **/

public class Customer {

    public static void main(String[] args) throws IOException, TimeoutException {
        /*//创建连接mq的连接工厂对象
        ConnectionFactory connectionFactory = new ConnectionFactory();
        //设置连接rbmq的主机
        connectionFactory.setHost("116.62.131.109");
        //设置端口
        connectionFactory.setPort(5672);
        //设置连接那个虚拟机
        connectionFactory.setVirtualHost("/ems");
        //设置访问虚拟主机的用户名密码
        connectionFactory.setUsername("ems");
        connectionFactory.setPassword("123");
        //获取连接对象
        Connection connection = connectionFactory.newConnection();*/

        //获取连接
        Connection connection = RbmqUtil.getConnection();
        //获取连接中通道
        Channel channel = connection.createChannel();

        channel.queueDeclare("hello",false, false,false,null);

        channel.basicConsume("hello",true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(LocalDateTime.now()+"--"+new String(body));
            }
        });

    }
}
