package com.wu.t3;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.wu.utils.RbmqUtil;

import java.io.IOException;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-07 20:54
 **/

public class Provider {
    public static void main(String[] args) throws IOException {
        Connection connection = RbmqUtil.getConnection();
        Channel channel = connection.createChannel();
        //声明交换机
        //参数1:交换机的名称  参数2: fanout为广播
        channel.exchangeDeclare("logs","fanout");
        //发布消息
        channel.basicPublish("logs","",null,"这是一条广播信息!!!".getBytes());

        RbmqUtil.closeChannelAndConnection(channel,connection);

    }
}
