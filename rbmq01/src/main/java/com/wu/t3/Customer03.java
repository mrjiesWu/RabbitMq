package com.wu.t3;

import com.rabbitmq.client.*;
import com.wu.utils.RbmqUtil;

import java.io.IOException;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-07 20:54
 **/

public class Customer03 {

    public static void main(String[] args) throws IOException {
        Connection connection = RbmqUtil.getConnection();
        Channel channel = connection.createChannel();
        //绑定交换机
        channel.exchangeDeclare("logs","fanout");
        //创建临时队列
        String queue = channel.queueDeclare().getQueue();
        //将临时队列绑定exchange
        channel.queueBind(queue,"logs","");
        //处理消息
        channel.basicConsume(queue,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("消费者03---"+new String(body));
            }
        });
    }
}
