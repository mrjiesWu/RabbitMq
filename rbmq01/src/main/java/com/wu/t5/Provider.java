package com.wu.t5;

import com.rabbitmq.client.*;
import com.wu.utils.RbmqUtil;

import java.io.IOException;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-09 19:35
 **/

public class Provider {
    public static void main(String[] args) throws IOException {
        Connection connection = RbmqUtil.getConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("topics","topic");
        channel.basicPublish("topics","user.save.2",null,"这是一条tops信息".getBytes());
        RbmqUtil.closeChannelAndConnection(channel,connection);
    }
}
