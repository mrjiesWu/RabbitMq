package com.wu.t5;

import com.rabbitmq.client.*;
import com.wu.utils.RbmqUtil;

import java.io.IOException;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-09 19:34
 **/

public class Customer02 {

    public static void main(String[] args) throws IOException {
        Connection connection = RbmqUtil.getConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("topics", "topic");
        //创建临时队列
        String queue = channel.queueDeclare().getQueue();
        //    #  匹配一个或多个单词
        channel.queueBind(queue, "topics", "user.#");
        channel.basicConsume(queue,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("-----" + new String(body));
            }
        });
    }
}
