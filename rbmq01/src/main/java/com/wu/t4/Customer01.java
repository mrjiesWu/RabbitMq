package com.wu.t4;

import com.rabbitmq.client.*;
import com.wu.utils.RbmqUtil;

import java.io.IOException;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-09 15:51
 **/public class Customer01 {
    public static void main(String[] args) throws IOException {
        Connection connection = RbmqUtil.getConnection();
        Channel channel = connection.createChannel();

        String exchange = "logs_direct";
        String routKey = "log";
        channel.exchangeDeclare(exchange,"direct");
        //创建临时队列
        String queue = channel.queueDeclare().getQueue();
        channel.queueBind(queue,exchange,routKey);

        channel.basicConsume(queue,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("消费者--1--"+new String(body));
            }
        });

    }
}


