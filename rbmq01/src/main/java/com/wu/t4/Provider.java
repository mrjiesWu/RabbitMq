package com.wu.t4;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.wu.utils.RbmqUtil;

import java.io.IOException;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-09 16:16
 **/

public class Provider {

    public static void main(String[] args) throws IOException {

        Connection connection = RbmqUtil.getConnection();

        Channel channel = connection.createChannel();
        channel.exchangeDeclare("logs_direct","direct");
        String routKey = "news";
        channel.basicPublish("logs_direct",routKey,null,"这是一条日志信息".getBytes());

        RbmqUtil.closeChannelAndConnection(channel,connection);

    }

}
