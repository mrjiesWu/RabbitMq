package com.wu.t4;

import com.rabbitmq.client.*;
import com.wu.utils.RbmqUtil;

import java.io.IOException;

/**
 * @program: RabbitMq
 * @author: Mr-Jies
 * @create: 2020-06-09 15:51
 **/

public class Customer02 {
    public static void main(String[] args) throws IOException {
        Connection connection = RbmqUtil.getConnection();
        Channel channel = connection.createChannel();

        String exchange = "logs_direct";
        String routKey = "logs";
        channel.exchangeDeclare(exchange,"direct");
        //创建临时队列
        String queue = channel.queueDeclare().getQueue();
        channel.queueBind(queue,exchange,routKey);
        channel.queueBind(queue,exchange,"news");

        channel.basicConsume(queue,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("消费者--2--"+new String(body)+"=-----="+envelope.getRoutingKey());
            }
        });

    }
}
